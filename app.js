const PORT = process.env.PORT || 5000;
var app = require('express')();
var ws = require('express-ws')(app);
var clients = [];

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.ws('/', (socket, req) => {
    clients.push(socket);

    console.log('a user connected');

    socket.on('message', function (msg) {
        for (var i = 0; i < clients.length; i++) {
            clients[i].send(msg);
        }
    });

    socket.on('close', function () {
        console.log('user disconnected');
        delete clients[clients.length - 1];
    });
});

app.listen(PORT, function () {
    console.log(`Example app listening on port ${PORT}!`);
});